const express = require('express');
const database = require('./db_escolares_pruebas');
const app = express();
const port = 3000;

const escolarespruebas = express.Router();

escolarespruebas.get('/expediente/:exp', function (req, res) {
    const expediente = req.params.exp;
    database.conectarBaseDeDatos((err, conn) => {
        conn.query('SELECT  expediente, unidadacademica, matricula ' +
            'FROM expediente WHERE expediente = ?;', [expediente], (err, data) => {
                if (err) {
                    console.error('Error al ejecutar la consulta:', err);
                    res.status(500).send('Error al ejecutar la consulta');
                } else {
                    // Enviar resultados como respuesta
                    res.json(data);
                    // Cerrar la conexión
                    conn.close((err) => {
                        if (err) {
                            console.error('Error al cerrar la conexión:', err);
                        } else {
                            console.log('Conexión cerrada.');
                        }
                    });
                }
            });
    });
});

escolarespruebas.get('/totaltutoradosde/:num_emp', function (req, res) {
    const numEmp = req.params.num_emp;
    database.conectarBaseDeDatos((err, conn) => {
        conn.query(`SELECT distinct count(*) as totaltutoradosdb FROM tutorexpediente ` +
            `WHERE empleado = ${numEmp} and estado = 1 and tipo = 0;`, (err, data) => {
                if (err) {
                    console.error('Error al ejecutar la consulta:', err);
                    res.status(500).send('Error al ejecutar la consulta');
                } else {
                    // Enviar resultados como respuesta
                    res.json(data);
                    // Cerrar la conexión
                    conn.close((err) => {
                        if (err) {
                            console.error('Error al cerrar la conexión:', err);
                        } else {
                            console.log('Conexión cerrada.');
                        }
                    });
                }
            });
    });
});

escolarespruebas.get('/busca_tutor/:municipio/:matrAlumno', function (req, res) {
    const municipio = req.params.municipio;
    const matricula = req.params.matrAlumno;
    database.conectarBaseDeDatos((err, conn) => {
        conn.query('SELECT p.nombre, p.apellidopaterno, p.apellidopaterno, e.empleado ' +
            'FROM persona p JOIN empleado e ON p.persona = e.persona ' +
            'JOIN tutorexpediente te ON e.empleado = te.empleado ' +
            'JOIN expediente ex ON te.expediente = ex.expediente ' +
            `WHERE ex.municipiomatric = ${municipio} AND ex.matricula = ${matricula} ;`, (err, data) => {
                if (err) {
                    console.error('Error al ejecutar la consulta:', err);
                    res.status(500).send('Error al ejecutar la consulta');
                } else {
                    // Enviar resultados como respuesta
                    res.json(data);
                    // Cerrar la conexión
                    conn.close((err) => {
                        if (err) {
                            console.error('Error al cerrar la conexión:', err);
                        } else {
                            console.log('Conexión cerrada.');
                        }
                    });
                }
            });
    });
});

app.use('/escolares_pruebas', escolarespruebas);

// Iniciar el servidor
app.listen(port, () => {
    console.log(`Servidor escuchando en http://localhost:${port}`);
});