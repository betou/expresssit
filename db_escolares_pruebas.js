const informix = require('informixdb');

// Configuración de la conexión a la base de datos
const connectionOptions = {
    SERVER: 'desa',
    DATABASE: 'escolares_pruebas',
    HOST: '148.231.10.31',
    SERVICE: 1542,
    UID: 'usrtutor',
    PWD: 'Dgh29EobbV',
    PROTOCOL : "onsoctcp" 
};

// Función para conectar a la base de datos
function conectarBaseDeDatos(callback) {
    informix.open(connectionOptions, function (err, conn) {
        if (err) {
            console.error('Error al conectar con la base de datos:', err);
            callback(err, null);
        } else {
            console.log('Conexión establecida con éxito.');
            callback(null, conn);
        }
    });
}

module.exports = {
    conectarBaseDeDatos
};